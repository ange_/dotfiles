" Ange's ~/.vimrc

let mapleader = ","
if has('nvim')
	set clipboard=unnamedplus
else
	set clipboard=unnamed
endif

" Navigation
set number
set relativenumber
set ruler	" Always display the current cursor position 
set mouse=a

" Theme
set background=dark
colorscheme koehler
set listchars=tab:>-

" Indentation
set autoindent noexpandtab tabstop=4 shiftwidth=4
autocmd FileType sh let b:is_bash = 1 | syntax on
autocmd FileType c,cpp,java setlocal commentstring=//\ %s
autocmd FileType html setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType python set omnifunc=syntaxcomplete#Complete
"autocmd FileType css,scss set omnifunc=syntaxcomplete#CompleteCSS
"autocmd FileType php setlocal omnifunc=lsp#complete
filetype plugin indent on
syntax on

function Spaces_to_tabs()
	set tabstop=4	   " To match the sample file
	set noexpandtab    " Use tabs, not spaces
	%retab!		   " Retabulate the whole file
endfunction

" Macros
let @t = '0yypv$r='	" write as many = as the lines characters (markdown)

" Spell
set spelllang=it,en

" Mappings {{{
	" Buffer movements: Shift. Makes sense: lowercase makes you move inside the buffer.
	map <S-F> :0<CR>
	nnoremap <S-h> <C-w>h
	nnoremap <S-j> <C-w>j
	nnoremap <S-k> <C-w>k
	nnoremap <S-l> <C-w>l
	nnoremap <leader>bn :%bd<CR>	" Nuke every buffer
	nnoremap <leader>bnx :%bd <bar> e#<CR>	" Nuke every buffer except the current one.

	" Indentation: leader + i. Also included < as a test.
	nnoremap <leader>it :if &list ==# 1 <bar> set nolist <bar> else <bar> set list <bar> endif<CR>
	nnoremap <leader>ij :%!jq .<CR>
	nmap <leader>i<tab> :call Spaces_to_tabs()<CR>
	nnoremap <t :if &list ==# 1 <bar> set nolist <bar> else <bar> set list <bar> endif<CR>
	nnoremap <j :%!jq .<CR>
	nmap <<tab> :call Spaces_to_tabs()<CR>

	" Code: leader in normal mode.
	nnoremap <leader>ch :LspHover<CR>
	nnoremap <leader>ca :LspCodeAction<CR>
	nnoremap <leader>cf :LspSignatureHelp<CR>
	nnoremap <leader>cn :LspRename<CR>

	" Editor: Control.
	nnoremap <C-h> :noh<CR>	" clear highlighted text
	nnoremap <C-n> :put _<CR>	" add new line
	nnoremap <leader>n :put _<CR>	" add new line
	nnoremap <C-s> :if &spell ==# 1 <bar> set nospell <bar> else <bar> set spell <bar> endif<CR>
	nnoremap <leader>sp :if &spell ==# 1 <bar> set nospell <bar> else <bar> set spell <bar> endif<CR>
	inoremap <C-d> <C-r>=strftime('%d-%m-%Y')<CR>
	inoremap <C-d><C-d> <C-r>=strftime('%a')<CR>
	nmap <C-b> :CtrlPBuffer<CR>
	nmap <leader>b :ls<CR>:b<Space>
"}}}

" Plugins {{{
	" vim-lsp
	augroup LSP
		if executable('clangd')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'clangd',
				\ 'cmd': {server_info->['clangd', '--enable-config']},
				\ 'whitelist': ['c', 'cpp', 'tpp'],
			\ })
		endif
		
		if executable('pylsp')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'pylsp',
				\ 'cmd': {server_info->['pylsp']},
				\ 'whitelist': ['python'],
			\ })
		endif

		if executable('jedi-language-server')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'jedi-language-server',
				\ 'cmd': {server_info->['jedi-language-server']},
				\ 'whitelist': ['python'],
			\ })
		endif

		if executable('solargraph')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'solargraph',
				\ 'cmd': {server_info->['solargraph', 'stdio']},
				\ 'whitelist': ['ruby'],
			\ })
		endif

		if executable('cmake-language-server')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'cmake',
				\ 'cmd': {server_info->['cmake-language-server']},
				\ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'build/'))},
				\ 'whitelist': ['cmake'],
				\ 'initialization_options': {
				\   'buildDirectory': 'build',
				\ }
			\ })
		endif

		if executable('jsonnet-language-server')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'json',
				\ 'cmd': {server_info->['jsonnet-language-server']},
				\ 'whitelist': ['json']
			\ })
		endif

		if executable('erlang_ls')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'Erlang',
				\ 'cmd': {server_info->['erlang_ls']},
				\ 'whitelist': ['erlang']
			\})
		endif

		if executable('phpactor')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'PHP',
				\ 'cmd': {server_info->['phpactor', 'language-server']},
				\ 'whitelist': ['php']
			\})
		endif

		if executable('intelephense')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'Intelephense (PHP)',
				\ 'cmd': {server_info->['intelephense', '--stdio']},
				\ 'whitelist': ['php']
			\})
		endif

		if executable('sqls')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'SQL',
				\ 'cmd': {server_info->['sqls']},
				\ 'whitelist': ['sql']
			\})
		endif

		if executable('eclipse-jdt-ls')
			au User lsp_setup call lsp#register_server({
				\ 'name': 'Java',
				\ 'cmd': {server_info->['eclipse-jdt-ls']},
				\ 'whitelist': ['java']
			\})
		endif
	augroup END

	" SuperTab
	let g:SuperTabDefaultCompletionType = "<c-n>"
	let g:SuperTabContextDefaultCompletionType = "<c-n>"
	autocmd FileType css,scss let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
	autocmd FileType css,scss let g:SuperTabContextDefaultCompletionType = "<c-n>"

	" NERDTree
	let g:NERDTreeShowLineNumbers=1
	nnoremap <C-f> :NERDTreeToggle<CR>
	nnoremap <leader>f :NERDTreeToggle<CR>

	" Ale Linter
	let g:ale_linters = { 'c': ['clang', '--enable-config'], 'cpp': ['clang', '--enable-config'] }
	let g:ale_cpp_gcc_options = '-std=c++11 -I/home/alessandro/Qt/5.12.4/gcc_64/include/QtCore'
	let g:ale_cpp_clang_options = '-std=c++11 -I$HOME/Applications/Qt/5.12.4/clang_64/include/QtCore'
"}}}

" Unused plugins configurations {{{
	" CoC
	inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
	inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

	" Autocomplete: no preview window on top
	set completeopt-=preview

	" YCM
	let g:ycm_confirm_extra_conf = 0

	" Deoplete.
	let g:deoplete#enable_at_startup = 1
	if has('macunix')
		let g:deoplete#sources#clang#libclang_path = '/Library/Developer/CommandLineTools/usr/lib/libclang.dylib'
	else
		let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-14/lib/libclang.so'
	endif
	let g:deoplete#sources#clang#std = { 'c': 'c99', 'cpp': 'c++11' }
	let g:deoplete#sources#clang#flags = ["-Wall", "-Wextra",]
	let g:deoplete#sources#clang#include_default_arguments = 1

"}}}

"Allow per-machine configuration
if filereadable(expand('~/.vimrc.local'))
	source ~/.vimrc.local
endif
