# Ange's personal ~/.zshrc

### BASE CONFIG
setopt nobeep				# Avoid beeps
setopt auto_cd				# If the command does not exist, but a directory does, automatically cd into that dir.
setopt auto_pushd			# Make cd push the old directory onto the directory stack.
setopt pushd_ignore_dups	# Don't push the same dir twice.
setopt noglobdots			# Do not match dotfiles
setopt correct #correct_all	# Set autocorrection
# setopt noshwordsplit		# use zsh style word splitting
# setopt unset				# don't error out when unset parameters are used

# History
setopt HIST_IGNORE_DUPS		# Ignore duplicates
setopt append_history		# Append history to history file. Default, but we enforce it for share_history
setopt share_history		# Share history in other ZSH sessions
setopt extended_history		# Timestamp
setopt histignorespace		# Do not put command in history if starts with a space
setopt histignorealldups	# If a cmd duplicates an older one in the list, the older is removed
HISTFILE=${HISTFILE:-${ZDOTDIR:-${HOME}}/.zsh_history}
HISTSIZE=5000  
SAVEHIST=10000 

# Jobs
setopt longlistjobs			# display PID when suspending processes as well
setopt notify				# report the status of backgrounds jobs immediately
setopt nohup				# Don't send SIGHUP to background processes when the shell exits.
REPORTTIME=3				# report about cpu-/system-/user-time of command if running longer than 3 seconds
watch=(notme root)			# watch for everyone but me and root

# In order to use #, ~ and ^ for regex-search
#	^foo		--> Everything but foo
#	(#an)foobar --> Tries to match everything like 'foobar' or with n error 
# Don't forget to quote '^', '~' and '#'!
setopt extended_glob
setopt extendedglob
setopt no_nomatch			# If does not match an extendedglob, pass the string to the command
export NO_BAD_PATTERN		# ?

## ENVIRONMENT
(( ${+commands[nvim]} )) &&		# Setup EDITOR to nvim, or fallback to vim or vi
	export EDITOR=${EDITOR:-nvim} ||
		(( ${+commands[vim]} )) && 
		export EDITOR=${EDITOR:-vim} ||
		export EDITOR=${EDITOR:-vi}

export MAIL=${MAIL:-/var/mail/$USER}	# Mailchecks
MAILCHECK=30

export LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:';	# Taken from dircolors -b

export PAGER=${PAGER:-less}		# Use less as pager
# support colors in less
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'

typeset -U path cdpath fpath manpath	# automatically remove duplicates from these arrays


### AUTOCOMPLETION
COMPDUMPFILE=${COMPDUMPFILE:-${ZDOTDIR:-${HOME}}/.zcompdump}
autoload -Uz compinit
compinit -d ${COMPDUMPFILE}

setopt completeinword			# Do not complete just the end of the word

zstyle ':completion:*'					menu select											# allow arrows-interaction
zstyle ':completion:*:approximate:'		max-errors 'reply=( $((($#PREFIX+$#SUFFIX)/3 )) numeric )'	# allow one error for every three characters typed in approximate completer
zstyle ':completion:*:warnings'			format $'%{\e[0;31m%}No matches for:%{\e[0m%} %d'	# set format for warnings
zstyle ':completion:*'					verbose true										# provide verbose completion information
zstyle ':completion:*:complete:-command-::commands' ignored-patterns '(aptitude-*|*\~)'		# don't complete backup files as executables
zstyle ':completion:*:default'			list-colors ${(s.:.)LS_COLORS}						# activate color-completion
zstyle ':completion:*:descriptions'		format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'	# format on completion
zstyle ':completion:*'					matcher-list 'm:{a-z}={A-Z}'						# match uppercase from lowercase
zstyle ':completion:*:options'			description 'yes'									# describe options in full
zstyle ':completion:correct:'			prompt 'correct to: %e'
zstyle ':completion:*' special-dirs ..														# provide .. as a completion
zstyle ':completion::(^approximate*):*:functions' ignored-patterns '_*'						# Ignore completion functions for commands you don't have:
zstyle ':completion:*:processes-names' command 'ps c -u ${USER} -o command | uniq'			# Provide more processes in completion of programs like killall:
zstyle ':completion:*:urls' local 'www' '/var/www/' 'public_html' '$HOME/Sites'				# command for process lists, the local web server details and host completion
zstyle ':completion:*:processes'		command 'ps -au$USER'								# on processes completion complete all user processes
#zstyle ':completion:*:-command-:*:'	verbose false										# Disable command descriptions
zstyle ':completion:*'					rehash true											# Rehash after install/removal

# activate menu and ignore duplicate entries
zstyle ':completion:*:history-words'	menu yes
zstyle ':completion:*:history-words'	remove-all-dups yes
zstyle ':completion:*:history-words'	stop yes

# insert all expansions for expand completer
zstyle ':completion:*:expand:*'			tag-order all-expansions
zstyle ':completion:*:history-words'	list false

# separate matches into groups
zstyle ':completion:*:matches'			group 'yes'
zstyle ':completion:*'					group-name ''

# complete manual by their section
zstyle ':completion:*:manuals'			separate-sections true
zstyle ':completion:*:manuals.*'		insert-sections   true
zstyle ':completion:*:man:*'			menu yes select

# Search path for sudo completion
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin usr/local/bin \
							/usr/sbin /usr/bin /sbin /bin \
							/usr/X11R6/bin

# Unknown
# zstyle ':completion:*:messages'			format '%d'
# zstyle ':completion:*:options'			auto-description '%d'

# start menu completion only if it could find no unambiguous initial string
# zstyle ':completion:*:correct:*'		insert-unambiguous true
# zstyle ':completion:*:corrections'	format $'%{\e[0;31m%}%d (errors: %e)%{\e[0m%}'
# zstyle ':completion:*:correct:*'		original true

# Use cache
COMP_CACHE_DIR=${COMP_CACHE_DIR:-${ZDOTDIR:-$HOME}/.cache}
[[ ! -d ${COMP_CACHE_DIR} ]] && command mkdir -p "${COMP_CACHE_DIR}"
zstyle ':completion:*' use-cache  yes
zstyle ':completion:*:complete:*' cache-path "${GRML_COMP_CACHE_DIR}"

# Host completion
[[ -r ~/.ssh/config ]] && _ssh_config_hosts=(${${(s: :)${(ps:\t:)${${(@M)${(f)"$(<$HOME/.ssh/config)"}:#Host *}#Host }}}:#*[*?]*}) || _ssh_config_hosts=()
[[ -r ~/.ssh/known_hosts ]] && _ssh_hosts=(${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[\|]*}%%\ *}%%,*}) || _ssh_hosts=()
[[ -r /etc/hosts ]] && : ${(A)_etc_hosts:=${(s: :)${(ps:\t:)${${(f)~~"$(</etc/hosts)"}%%\#*}##[:blank:]#[^[:blank:]]#}}} || _etc_hosts=()
hosts=(
	$(hostname)
	"$_ssh_config_hosts[@]"
	"$_ssh_hosts[@]"
	"$_etc_hosts[@]"
	localhost
)
zstyle ':completion:*:hosts' hosts $hosts
compdef _hosts

# Completion with _gnu_generic with some commands
for compcom in cp deborphan df feh fetchipac gpasswd head hnb ipacsum mv \
				pal stow uname ; do
	[[ -z ${_comps[$compcom]} ]] && compdef _gnu_generic ${compcom}
done; unset compcom

### PROMPT: style taken from grml's prompt
autoload -Uz vcs_info

zstyle ':vcs_info:git:*' formats " %F{magenta}(%f%s%F{magenta})%f%F{yellow}-%f%F{magenta}[%f%F{green}%b%F{magenta}]%f"
zstyle ':vcs_info:git:*' actionformats " %F{magenta}(%f%s%F{magenta})%F{yellow}-%F{magenta}[%F{green}%b%F{yellow}|%F{red}%a%F{magenta}]%f"
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat "%b${red}:${yellow}%r"

setopt PROMPT_SUBST

PS1='%(?..%B%F{red}%?%f%b )%B%F{%(!,red,blue)}%n%f%b@%m %B%~%b${vcs_info_msg_0_} %# '
RPS1='%(?..:()'		# Sad smiley on exit code !0
PS2='\`%_> '		# When more info about command is needed
PS3='?# '			# Selection prompt used within a select loop
PS4='+%N:%i:%_> '	# the execution trace prompt (setopt xtrace). default: '+%N:%i>'

# Set terminal title to prompt
#DISABLE_AUTO_TITLE="true"
case $TERM in
xterm*)
	precmd () {
		vcs_info
		print -Pn "\e]0;%n@%m: %~\a"
	}
	;;
*)
	precmd() {
		vcs_info
	}
	;;
esac


### Functions
# Rename with regexp
autoload -U zmv

# smart cd function, allows switching to /etc when running 'cd /etc/fstab'
cd() {
	if (( ${#argv} == 1 )) && [[ -f ${1} ]]; then
		[[ ! -e ${1:h} ]] && return 1
		print "Correcting ${1} to ${1:h}"
		builtin cd ${1:h}
	else
		builtin cd "$@"
	fi
}

mkcd () {
	if (( ARGC != 1 )); then
		printf 'usage: mkcd <new-directory>\n' >&2
		return 1;
	fi
	if [ ! -d "$1" ]; then
		command mkdir -p "$1"
	else
		printf "'%s' already exists: cd-ing.\n" "$1"
	fi
	builtin cd "$1"
}

# List files which have been accessed within the last {\it n} days, {\it n} defaults to 1
accessed() {
	emulate -L zsh
	print -l -- *(a-${1:-1})
}

# List files which have been changed within the last {\it n} days, {\it n} defaults to 1
changed() {
	emulate -L zsh
	print -l -- *(c-${1:-1})
}

# List files which have been modified within the last {\it n} days, {\it n} defaults to 1
modified() {
	emulate -L zsh
	print -l -- *(m-${1:-1})
}

# Decompress any archive
decompress() {
	echo "To implement."
}

are_equal() {
	if [ "$1" = "$2" ]; then
		printf "Yes.\n"
		return 0
	else
		printf "No.\n"
		return 1
	fi
}

bak() {
	if (( ${#argv} < 1 )); then
		printf "Usage: bak [-c] [FILE(s)]\n" >&2
		return 1
	fi

	CMD="mv"
	while getopts ch opt; do
		case $opt in
			"c")
				CMD="cp"
				;;
			"h")
				printf "Usage: bak [-c] [FILE(s)]\n"
				return
				;;
		esac
	done
	shift "$((OPTIND - 1))"

	for i in $@; do
		$CMD -i $i $i.backup
	done
}

unbak() {
	if (( ${#argv} < 1 )); then
		printf "Usage: unbak [FILE(s)]\n" >&2
		return 1
	fi

	while getopts h opt; do
		case $opt in
			"h")
				printf "Usage: unbak [FILE(s)]\n"
				return
				;;
		esac
	done
	shift "$((OPTIND - 1))"

	for i in $@; do
		mv $i ${i%.backup}
	done
}

mping() {
	k=$(ping 8.8.8.8 -c 1 | awk 'FNR == 2 { gsub("time=",""); print $(NF-1); }')
	if [ "$?" -ne 0 ]; then
		printf "No connection.\n" >&2
		return 1
	else
		printf "%f ms\n" "$k"
	fi
}


raw_github() {
	url="$1"
	raw_url="$(echo "$url" | sed -e 's/github./raw.githubusercontent./' -e 's/\/blob//')"
	echo "$raw_url"
}

pow() {
	if [[ ${#argv} -ne 2 ]]; then
		printf "Usage: pow [NUMBER] [EXPONENT]\n" >&2
		return 1
	fi

	number=$1
	exponent=$2
	ret=1
	isNegative=false

	if [[ $exponent -lt 0 ]]; then
		exponent=$(($exponent * (-1)))
		isNegative=true
	fi

	while [[ $exponent -ge 1 ]]; do
		let ret=$(($ret * $number))
		exponent=$exponent-1
	done

	if $isNegative; then
		printf "1/"
	fi
	printf "%s\n" "$ret"
	return 0
}
# Meme
spongeprint() {
	upper=false
	K="$1"
	ret=""
	for i in $(seq 0 ${#K}); do
		cchar=${K:$i:1}
		if [ "$cchar" = ' ' ]; then
			ret+=' '
		elif [ "$cchar" = 'i' ] || [ "$cchar" = 'I' ]; then
			ret+='i'
		elif [ "$cchar" = 'L' ] || [ "$cchar" = 'l' ]; then
			ret+='L'
		elif $upper; then
			ret+=$(tr '[:lower:]' '[:upper:]' <<< "$cchar")
			upper=false
		else
			ret+=$(tr '[:upper:]' '[:lower:]' <<< "$cchar")
			upper=true
		fi
	done
	[ "$(uname)" = 'Darwin' ] && pbcopy <<< "$ret"
	echo "$ret"
}

drm() {
	for dir in $@; do
		[ ! -d "$dir" ] && echo "\"$dir\" is not a directory." >&2 && return 1
	done

	builtin rm -r "$@"
}

if (( ${+commands[git]} )); then
	gc() {
		git clone https://github.com/$1
	}

	alias gs='git status'
	alias gd='git diff'
	alias gl='git log'
fi


### ZLE EXTENSIONS
# only slash should be considered as a word separator:
slash-backward-kill-word() {
	local WORDCHARS="${WORDCHARS:s@/@}"
	# zle backward-word
	zle backward-kill-word
}
zle -N slash-backward-kill-word

autoload -U edit-command-line && zle -N edit-command-line
if autoload -U history-search-end; then
	zle -N history-beginning-search-backward-end history-search-end
	zle -N history-beginning-search-forward-end  history-search-end
fi

# Edit alias using ZLE
edalias() {
	[[ -z "$1" ]] && { echo "Usage: edalias <alias_to_edit>" ; return 1 } || vared aliases'[$1]' ;
}

# Edit function using ZLE
edfunc() {
	[[ -z "$1" ]] && { echo "Usage: edfunc <function_to_edit>" ; return 1 } || zed -f "$1" ;
}

insert-datestamp() { LBUFFER+=${(%):-'%D{%Y-%m-%d}'}; }
zle -N insert-datestamp

insert-last-typed-word() { zle insert-last-word -- 0 -1 };
zle -N insert-last-typed-word;

function grml-zsh-fg() {
	if (( ${#jobstates} )); then
		zle .push-input
		[[ -o hist_ignore_space ]] && BUFFER=' ' || BUFFER=''
		BUFFER="${BUFFER}fg"
		zle .accept-line
	else
		zle -M 'No background jobs. Doing nothing.'
	fi
}
zle -N grml-zsh-fg

sudo-command-line() {
	[[ -z $BUFFER ]] && zle up-history
	if [[ $BUFFER != sudo\ * ]]; then
		BUFFER="sudo $BUFFER"
		CURSOR=$(( CURSOR+5 ))
	fi
}
zle -N sudo-command-line

function jump_after_first_word() {
	local words
	words=(${(z)BUFFER})

	if (( ${#words} <= 1 )) ; then
		CURSOR=${#BUFFER}
	else
		CURSOR=${#${words[1]}}
	fi
}
zle -N jump_after_first_word

zleiab() {
	emulate -L zsh
	setopt extendedglob
	local MATCH

	LBUFFER=${LBUFFER%%(#m)[.\-+:|_a-zA-Z0-9]#}
	LBUFFER+=${abk[$MATCH]:-$MATCH}
}

help-show-abk() {
	zle -M "$(print "Available abbreviations for expansion:"; print -a -C 2 ${(kv)abk})"
}

zle -N zleiab 
zle -N help-show-abk

slash-backward-kill-word() {
	local WORDCHARS="${WORDCHARS:s@/@}"
	# zle backward-word
	zle backward-kill-word
}
zle -N slash-backward-kill-word

zle -C hist-complete complete-word _generic
zstyle ':completion:hist-complete:*' completer _history

autoload -U insert-files edit-command-line
zle -N insert-files
zle -N edit-command-line

### "hash -d" commonly used directories
# hash -d /var/log
# Hash command at completion
setopt hash_list_all


### KEYMAPS
typeset -A key
key=(
	Home	 "${terminfo[khome]}"
	End		 "${terminfo[kend]}"
	Insert	 "${terminfo[kich1]}"
	Delete	 "${terminfo[kdch1]}"
	Up		 "${terminfo[kcuu1]}"
	Down	 "${terminfo[kcud1]}"
	Left	 "${terminfo[kcub1]}"
	Right	 "${terminfo[kcuf1]}"
	PageUp	 "${terminfo[kpp]}"
	PageDown "${terminfo[knp]}"
	BackTab  "${terminfo[kcbt]}"
)

bind2maps() {
	local i sequence widget
	local -a maps

	while [[ "$1" != "--" ]]; do
		maps+=( "$1" )
		shift
	done
	shift

	if [[ "$1" == "-s" ]]; then
		shift
		sequence="$1"
	else
		sequence="${key[$1]}"
	fi
	widget="$2"

	[[ -z "$sequence" ]] && return 1

	for i in "${maps[@]}"; do
		bindkey -M "$i" "$sequence" "$widget"
	done
}

# If something has been typed, complete it.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
	function zle-smkx () {
		emulate -L zsh
		printf '%s' ${terminfo[smkx]}
	}
	function zle-rmkx () {
		emulate -L zsh
		printf '%s' ${terminfo[rmkx]}
	}
	function zle-line-init () {
		zle-smkx
	}
	function zle-line-finish () {
		zle-rmkx
	}
	zle -N zle-line-init
	zle -N zle-line-finish
else
	for i in {s,r}mkx; do
		(( ${+terminfo[$i]} )) || grml_missing_features+=($i)
	done
	unset i
fi


zmodload zsh/terminfo
bindkey -e
bindkey -M emacs $key[Insert] overwrite-mode
bindkey -M viins $key[Insert] overwrite-mode
bindkey -M vicmd $key[Insert] vi-insert
bindkey -M emacs $key[Left] backward-char
bindkey -M emacs $key[Right] forward-char
bindkey -M emacs $key[Delete] delete-char
bindkey -M vicmd $key[Delete] vi-delete-char
bindkey -M viins $key[Delete] vi-delete-char
bind2maps emacs viins vicmd -- -s '^[[1;3C' forward-word
bind2maps emacs viins vicmd -- -s '^[[1;3D' backward-word	# <A-left arrow>
bind2maps emacs viins vicmd -- -s '^[[1;5D' backward-word	# <C-left arrow>
bind2maps emacs viins vicmd -- -s '^[[1;5C' forward-word

# Line editing-related
bind2maps emacs viins		-- -s '\e^?' slash-backward-kill-word	# <E-Del> to remove word
bind2maps emacs viins		-- -s '\em' insert-last-typed-word
bind2maps emacs viins		-- -s '^os' sudo-command-line
bind2maps emacs viins		-- -s '^x1' jump_after_first_word
bind2maps emacs viins		-- -s '^xf' insert-files
bind2maps emacs viins		-- -s '\ee' edit-command-line

# Functionalities
bindkey -M emacs $key[BackTab] reverse-menu-complete
bindkey -M viins $key[BackTab] reverse-menu-complete
bind2maps emacs viins		-- '^ed' insert-datestamp
bind2maps emacs viins		-- -s '^z' grml-zsh-fg
bind2maps emacs viins		-- -s ' ' magic-space	# history expansion on space
#bind2maps emacs viins		-- -s '\ei' menu-complete  # menu completion via esc-i

# Abbreviations
bind2maps emacs viins		-- -s '^x.' zleiab
bind2maps emacs viins		-- -s '^xb' help-show-abk

# History
bind2maps emacs viins		-- PageUp	history-beginning-search-backward-end
bind2maps emacs viins		-- PageDown	history-beginning-search-forward-end
bind2maps emacs viins vicmd -- Up		up-line-or-search
bind2maps emacs viins vicmd -- Down		down-line-or-search
bind2maps emacs viins		-- -s "^x^x" hist-complete	# complete word from history with menu


### SHORTHANDS w/ "<C-x>."
declare -A abk
setopt interactivecomments
abk=(
	'L'	'| less -r'
	'ma' 'make && make install'
	'co' './configure --prefix=/usr/local && make'
)


zmodload -a zsh/stat zstat


### ALIASES
alias rm='rm -i'
alias c='clear'
alias l='ls'
alias vimedit="$EDITOR ~/.vimrc"
alias zshed="$EDITOR ~/.zshrc"
alias q='exit'
alias grep='grep --color=always'
alias cdtemp='cd "$(mktemp -d)"'

export BOLD="\033[1m"
export UNBOLD="\033[0m"

[ -d "$HOME/bin" ] && export PATH="$HOME/bin:$PATH"
[ -d "$HOME/.local/bin" ] && export PATH="$HOME/.local/bin:$PATH"
[ -d "$HOME/Qt" ] && export QT_HOME="$HOME/Qt" && alias qmake="$QT_HOME/*/clang_64/bin/qmake"
(( ${+commands[nvim]} )) && alias vi=nvim

local CFLAGS="-O3 -mtune=native -march=native -Wall -Wextra -fshort-enums"
local CXXFLAGS="$CFLAGS -Weffc++" 

case "$(uname)" in
	"Darwin")
		export CLICOLOR=1	# color setup for ls on OS X / FreeBSD:
		local_python="$(find "$HOME/Library/Python" -type d -name bin 2>/dev/null | tail -1)"
		[ -n "$local_python" ] && export PATH="$PATH:$local_python"
		alias ls="ls -G"
		alias less='less -r'
		alias temp='sudo powermetrics --samplers smc -i1 -n1 |grep -i "CPU die temperature"'
		alias java_home='/usr/libexec/java_home -F'
		alias ccopy='pbcopy'
		[ -d "/Applications/Sublime Text.app" ] && alias subl='/Applications/Sublime\ Text.app/Contents/MacOS/Sublime\ Text'
		[ -d "$HOME/Applications/Sublime Text.app" ] && alias subl="$HOME/Applications/Sublime\ Text.app/Contents/MacOS/sublime_text"
		alias sublime='open -a Sublime\ Text'
		[ -x /Applications/CMake.app/Contents/bin/cmake ] && alias cmake='/Applications/CMake.app/Contents/bin/cmake'
		[ -d /opt/vim/root/bin/ ] && alias vim=/opt/vim/root/bin/vim
		[ -d /Applications/Postgres.app ] && export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/latest/bin

		(( ${+commands[brew]} )) && export HOMEBREW_NO_AUTO_UPDATE=1
		unibz="$HOME/Documents/UniBZ"

		# Broken down key: use right arrow.
		bind2maps emacs viins vicmd -- Right	down-line-or-search

		timer() {
			if (( ${#argv} < 2 )); then
				printf 'usage: timer [TIME] [SENTENCE]\n' >&2
				return 1
			fi
			time=$1
			sentence=$2
			for i in $(seq $(($time * 60)) 1); do
				printf "$i \r"
				sleep 1
			done && printf "\n"; osascript -e "display notification \"$sentence\""
		}

		pasta() {
			if (( ${#argv} < 1 )); then
				printf 'usage: pasta [TIME]\n' >&2
				return 1
			fi
			timer $1 "Scola la pasta"
		}

		MAMP() {
			if ! which mysql.server &>/dev/null; then
				printf "Missing dependency: MySQL.\n" >&2
				return 2
			fi
			case $1 in
				"start")
					brew services start mysql@5.7
					sudo apachectl start
					[ $? -ne 0 ] && printf "Failed to start MAMP.\n" >&2 && return 1
					return 0
					;;
				"stop")
					brew services stop mysql@5.7
					sudo apachectl stop
					[ $? -ne 0 ] && printf "Failed to stop MAMP.\n" >&2 && return 1
					return 0
					;;
				"help")
					printf "Usage: %s [start/stop/help/status/password]\n" "MAMP"
					return 0
					;;
				"status")
					pgrep httpd &>/dev/null && echo "Web server on" || echo "Web server off"
					mysql.server status
					return 0
					;;
				"password")
					printf "pwd\n"
					return 0
					;;
				*)
					printf "Usage: %s [start/stop/help/status/password]\n" "MAMP"
					return 1
					;;
			esac
		}
		alias mamp=MAMP

		restart_siri() {
			ps aux | grep 'coreaudio[a-z]' | awk '{ print $1}'
		}

		macchanger() {
			device=${1:-en0}
			generateMAC() {
				ran() { echo $((RANDOM%256));}
				# ensure the most significant digit is valid i.e., even in decimal form
				m=$(ran)
				(( m % 2 != 0 )) && m=$((m+1))
				printf '%02X:%02X:%02X:%02X:%02X:%02X\n' \
					"$m" "$(ran)" "$(ran)" "$(ran)" "$(ran)" "$(ran)"
			}

			newmac=${2:-$(generateMAC)}
			echo "$newmac"
			sudo ifconfig "$device" ether "$newmac" && printf "%s\n" "$newmac"
		}
		;;

	"Linux")
		alias ls='ls --color=auto'
		alias systemctl='sudo systemctl'
		alias ccopy='xsel -b'
		if [ -f /etc/debian_version ]; then
			alias apt='sudo apt'
			alias apt-get='sudo apt-get'
			alias ifconfig='sudo ifconfig'
		fi

		# Broken down key: use right arrow.
		[ -n "$BROKEN_R_ARROW_KEY" ] && bind2maps emacs viins vicmd -- Right	down-line-or-search

		if [ "$(uname -m)" = 'armv7l' ]; then
			alias temp="vcgencmd measure_temp | sed \"s/temp=//; s/'/°/g\""
			(( ${+commands[apt-get]} )) && alias apt-get='sudo apt-get'
			(( ${+commands[apt]} )) && alias apt='sudo apt'
			(( ${+commands[raspi-config]} )) && alias raspi-config='sudo raspi-config'
			alias poweroff='sudo poweroff'
			alias reboot='sudo reboot'

			CFLAGS="${CFLAGS//-march=native }"
			CXXFLAGS="$CFLAGS"

		elif [ -n "$SCHOOL" ]; then
			# Custom aliases
			alias ranger="ranger.py"
			alias check_sam="grep \"sam\" /etc/passwd"
			which sublime_text &>/dev/null && alias sublime="sublime_text"

			# Custom functions
			set_theme() {
				gsettings set org.gnome.desktop.wm.preferences theme "$1"
				gsettings set org.gnome.desktop.interface gtk-theme "$1"
			}
		fi
		;;
esac

# Development tools setup
alias clang="clang $CFLAGS"
alias clang++="clang++ -std=c++14 $CXXFLAGS"
alias clang++11="clang++ -std=c++11 $CXXFLAGS"

alias gcc="gcc $CFLAGS"
alias g++="g++ $CXXFLAGS"

if (( ${+aliases[gcc]} )); then
	alias clang="gcc $CFLAGS"
	alias clang++="g++ $CXXFLAGS"
	alias clang++11="clang++ -std=c++11"
fi

[ -d "$HOME/.rbenv" ] && eval "$(~/.rbenv/bin/rbenv init - zsh)"

[ -f ~/.zshrc.local ] && . ~/.zshrc.local || return 0
export PATH=$PATH:$HOME/bin
