#!/bin/sh

TARGET_DIR="$HOME/.local/opt/jdt"
LATEST_ARCHIVE_URL='https://www.eclipse.org/downloads/download.php?file=/jdtls/snapshots/jdt-language-server-latest.tar.gz'

[ ! -d "$TARGET_DIR" ] && mkdir -p "$TARGET_DIR"
curl -Ls "$LATEST_ARCHIVE_URL" | tar xv - -C "$HOME/.local/opt/jdt"
