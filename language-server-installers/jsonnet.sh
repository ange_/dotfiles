#!/bin/sh

SCRIPT_DIR="$(realpath "$0" | xargs dirname)"

"$SCRIPT_DIR/jsonnet.rb" || (echo 'Unable to download jsonnet-language-server' >&2 && exit 1)
if [ ! -d "$HOME/.local/bin" ]; then
	mkdir -p "$HOME/.local/bin"
	[ "$?" -ne 0 ] && echo 'Fatal error: unable to create ~/.local/bin' >&2 && exit 1
fi

mv jsonnet-language-server "$HOME/.local/bin"
