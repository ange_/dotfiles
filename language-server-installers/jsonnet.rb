#!/usr/bin/env ruby
# frozen_string_literal: true

require 'RbConfig'
require 'net/http'
require 'open-uri'
require 'json'

target_os = RbConfig::CONFIG['host_os'].gsub(/\d/, '')
target_arch = RbConfig::CONFIG['host_cpu'].match?('x86_64') ? 'amd64' : 'i386'

url = URI('https://api.github.com/repos/grafana/jsonnet-language-server/releases/latest')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true # Use HTTPS
request = Net::HTTP::Get.new url
response = http.request request

if response.code == '200'
  data = JSON.parse(response.body)
  data['assets'].each do |asset|
    if asset['name'].include?("#{target_os}_#{target_arch}")
      exec_name = asset['name'].split('_')[0]
      puts asset['browser_download_url']
      content = URI.parse(asset['browser_download_url']).read
      File.open(exec_name, 'wb') do |file|
        file.write(content)
        file.chmod 0o755
      end
    end
  end
else
  puts "HTTP request failed with status #{response.code}"
  exit 1
end
