@echo off
SETLOCAL

(
	echo set runtimepath^^=~/dotfiles/.vim runtimepath+=~/dotfiles/.vim/after
	echo let ^&packpath=^&runtimepath
	echo source ~/dotfiles/.vimrc
) > %APPDATA%\..\Local\nvim\init.vim

ENDLOCAL
