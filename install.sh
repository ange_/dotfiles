#!/bin/sh

script_path="$(realpath "$(dirname "$0")")"

printf "Installing vim...\r"
if [ -e "$HOME/.vimrc" ]; then
	if [ "$(readlink -f "$HOME/.vimrc")" != "$script_path/.vimrc" ]; then
		mv "$HOME/.vimrc" "$HOME/.vimrc.origin"
		ln -s "$script_path/.vimrc" "$HOME/.vimrc"
	fi
else
	ln -s "$script_path/.vimrc" "$HOME/.vimrc"
fi

[ -d "$HOME/.vim/" ] && [ "$(readlink -f "$HOME/.vim/")" != "$script_path/.vim/" ] && mv "$HOME/.vim" "$HOME/.vim.origin"
ln -s "$(pwd)/.vim" "$HOME/.vim"
echo "vim installed!    "

if command -v nvim >/dev/null 2>&1; then
	nvim_config="$HOME/.config/nvim/"
	[ ! -d "$nvim_config" ] && mkdir -p "$nvim_config"
	[ -f "$nvim_config/init.vim" ] && mv "$nvim_config/init.vim" "$nvim_config/init.vim.origin"
	cat <<- EOF > "$HOME/.config/nvim/init.vim"
	set runtimepath^=~/.vim runtimepath+=~/.vim/after
	let &packpath=&runtimepath
	source ~/.vimrc
	EOF

	case "$(uname)" in
		"Linux")
			if [ -f /etc/debian_version ]; then
				sudo apt-get install -y python3-pynvim
				sudo apt-get install -y python3-clang	# Deoplete
			fi
			;;
		"Darwin")
			pip3 install --user pynvim
			;;
	esac

	# Required packages for LSP
	command -v pip3 >/dev/null 2>&1 || command -v pip >/dev/null 2>&1 &&
		pip3() {
			pip "$@"
		}
	pip3 install --user python-lsp-server
	pip3 install --user cmake-language-server
	gem install solargraph
fi

printf "Configuring Z Shell...\r"
chsh -s /bin/zsh "$USER"
if [ -f "$HOME/.zshrc" ] && ! readlink "$HOME/.zshrc" || [ -L "$HOME/.zshrc" ] && [ "$(readlink -f "$HOME/.zshrc")" != "$PWD/.zshrc" ]; then
	mv "$HOME/.zshrc" "$HOME/.zshrc.origin"
fi
ln -s "$(pwd)/.zshrc" "$HOME/.zshrc"
echo "Z Shell configured successfully."

printf "Configuring git...\r"
git config credential.helper store
git config --global core.pager 'less -r --mouse'
git config --global alias.onelog 'log --oneline'
git config --global alias.tree 'log --graph --oneline --all --decorate'
git config --global alias.treedates "log --pretty=format:'%C(yellow)%h %Cred%ad %Cblue%an %Cgreen%d %Creset%s' --date=short --graph --all"
echo "Git configured successfully."

printf "Configuring clangd...\r"
case "$(uname)" in
	'Linux')
		ln -s "$script_path/clangd" "$HOME/.config/"
		;;
	'Darwin')
		ln -s "$script_path/clangd" "$HOME/Library/Preferences/"
		;;
esac
echo "clangd configured with exit status: $?."

if [ "$(uname)" = 'Darwin' ]; then
	printf 'Installing ~/.inputrc\n'
	ln -s "$script_path/inputrc" "$HOME/.inputrc"
fi
