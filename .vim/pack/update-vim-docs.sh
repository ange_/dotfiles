#!/bin/sh

VIM_DIR="$(realpath "$0" | xargs dirname)/.."

for module in "$VIM_DIR/pack/"*/; do
	for plugin in "$module"/*/*/; do
		plugin_name="$(basename "$plugin")"
		[ "$plugin_name" = 'loremipsum' ] && continue
		echo "Updating $plugin_name docs"
		vim -u NONE -c "helptags $plugin/doc" -c q
	done
done
